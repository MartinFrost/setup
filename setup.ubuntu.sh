#!/usr/bin/env bash

echo "Installing ansible"
sudo apt install -y ansible python-lxml python-psutil

echo "Installing ansible galaxy community.general collection"
ansible-galaxy collection install community.general

echo "Setting up dotfiles"
ansible-playbook -i inventory ansible-playbooks/dotfiles.yml

echo "Running install script"
ansible-playbook -i inventory ansible-playbooks/ubuntu.yml

echo "Setting up custom keyboard layout"
ansible-playbook -i inventory ansible-playbooks/keyboard-layout.yml
