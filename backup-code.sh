#!/usr/bin/env bash
set -euo pipefail

tar zc \
    --exclude code/github/kundo/operations/docker-compose/mysql-data \
    --exclude "*/.elixir_ls*" \
    --exclude "*/node_modules*" \
    --exclude "*/_build/*" \
    --exclude "*/deps/*" \
    -f code.tgz code
