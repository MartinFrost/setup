# Setup

This are some scripts to set up new machines

## Arch Linux

Run setup if you have other setup

* Add yourself to the `docker` group

    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker

* Add Pyenv and add pyenv-default-packages:

    git clone https://github.com/jawshooah/pyenv-default-packages.git $(pyenv root)/plugins/pyenv-default-packages 

## Ubuntu

    ./setup.ubuntu.sh

## TODO

* Använd prebuilt erlang paket för Ubuntu

https://github.com/michallepicki/asdf-erlang-prebuilt-ubuntu-20.04
